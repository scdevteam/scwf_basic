# README #

ScorpioCoding Web Workflow Basic Starterkit

### What is this repository for? ###

* Web Workflow Basic Starterkit for building basic websites
* Html, html partials, html minify
* Js, Js Concat (main.js), Js minify, jQuery
* Css
* v1.0.0

* node - v8.9.4
* npm -  v5.6.0

[![dependencies](https://david-dm.org/scorpiocoding/scwf_basic.svg)](https://david-dm.org/scorpiocoding/QuickStart)
[![dev.dependencies](https://david-dm.org/scorpiocoding/QuickStart/dev-status.svg)](https://david-dm.org/scorpiocoding/QuickStart)

### How to use in terminal ###

1. ('BUILD')
prompt > gulp ('default') 
    * Copies all html, html-partials                --> Dir BUILD
    * Copies & Concats all js (javascript)          --> Dir BUILD
    * Copies all css (styleSheets)                  --> Dir BUILD
    * Copies all images                             --> Dir BUILD
    * Runs browserSync -> Serve (localhost)
    * Runs watch (change on file modification)

2. ('RELEASE')
prompt > gulp release 
    * Copies & minifies html, html-partials         --> Dir RELEASE
    * Copies & Concats & Minifies js (javascript)   --> Dir RELEASE
    * Copies & minifies css (styleSheets)           --> Dir RELEASE
    * Copies & minifies images                      --> Dir RELEASE


### How do I get set up? ###

1. Clone this repo or download it from this website

2. Make sure you have the following globally installed upon your computer
    * [nodejs](https://nodejs.org) 
    * [git](https://git-scm.com) 
    * [gulp](https://gulpjs.com)
    
3. Create a new project directory ex. "MyWebSite"

4. Open the project directory 
    * prompt > "cd MyWebSite"

5. Clone the workflow to the new project directory
    * prompt > "git clone https://ScorpioCoding@bitbucket.org/scdevteam/scwf_basic.git "

6. Create a new Git Repo for your project @bitbucket or @gitHup

7. Point your project to your new Git Repo
    * prompt > "git remote set-url origin https://ScorpioCoding@bitbucket.org/scdevteam/mywebsite.git "
    * prompt > "git remote add upstream https://ScorpioCoding@bitbucket.org/scdevteam/mywebsite.git "
    * prompt > "git push origin master "
    * prompt > "git push --all "
    * prompt > " Everything up to date"

8. Use Npm to install the devDependencies of the workflow
    * prompt > npm install

9. Use Npm to Check if devDependencies are outdated
    * prompt > npm outdated

10. Use Npm to update devDependencies
    * prompt > npm update

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### Directory Structure ###

* src       [development folder]
    - partials
    - js
    - css
    - img
* build     [browserSync execute folder]
    - js
    - css
    - img
* release   [release folder]
    - js
    - css
    - img

### devDependencies ###

    "browser-sync": "^2.23.2",
    "graceful-fs": "^4.1.11",
    "gulp": "^3.9.1",
    "gulp-browserify": "^0.5.1",
    "gulp-clean": "^0.3.2",
    "gulp-concat": "^2.6.1",
    "gulp-cssmin": "^0.2.0",
    "gulp-htmlmin": "^4.0.0",
    "gulp-imagemin": "^4.1.0",
    "gulp-inject-partials": "^1.0.3",
    "gulp-minify": "^2.1.0",
    "gulp-newer": "^1.3.0",
    "gulp-rename": "^1.2.2",
    "gulp-series": "^1.0.2",
    "jquery": "^3.2.1",
    "run-sequence": "^2.2.1"

### Links ###

* [browser-sync](https://www.npmjs.com/package/browser-sync)
* [gulp](https://gulpjs.com/)
* [gulp-browserify](https://www.npmjs.com/package/gulp-browserify)
* [gulp-clean](https://www.npmjs.com/package/gulp-clean)
* [gulp-concat](https://www.npmjs.com/package/gulp-concat)
* [gulp-htmlmin](https://www.npmjs.com/package/gulp-htmlmin)
* [gulp-imagemin](https://www.npmjs.com/package/gulp-imagemin)
* [gulp-inject-partials](https://www.npmjs.com/package/gulp-inject-partials)
* [gulp-minify](https://www.npmjs.com/package/gulp-minify)
* [gulp-newer](https://www.npmjs.com/package/gulp-newer)
* [gulp-series](https://www.npmjs.com/package/gulp-series)
* [jquery](https://jquery.com/)
* [run-sequence](https://www.npmjs.com/package/run-sequence)
