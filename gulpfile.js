//---
// ScorpioCoding Web Workflow Basic Starterkit
// 2018-01-02 - v1.0.0 - kribo - created
//---

var gulp = require('gulp');
var clean = require('gulp-clean');
var series = require('gulp-series');
var runSequence = require('run-sequence');
var browserSync = require('browser-sync');
var relaod = browserSync.reload;
var injectPartials = require('gulp-inject-partials');
var htmlmin = require('gulp-htmlmin');
var cssmin = require('gulp-cssmin');
var jsmin = require('gulp-jsmin');
var browserify = require('gulp-browserify');
var rename = require('gulp-rename');
var newer = require('gulp-newer');
var imagemin = require('gulp-imagemin');
var minify = require('gulp-minify');
var concat = require('gulp-concat');


var PATH = {
    SRC:  { 
        src  : 'src',
        img  : 'src/img/**',
        html : 'src/*.html',
        css  : 'src/css/*.css',
        js   : 'src/js/*.js'
    },
    BUILD: {
        root : 'build',
        img  : 'build/img',
        css  : 'build/css',
        js   : 'build/js'

    },
    RELEASE: {
        root : 'release',
        img  : 'release/img',
        css  : 'release/css',
        js   : 'release/js'
    }
};

//---------------- BUILD ----------------------------------------------------------- 

series.registerTasks({
    "html:clean" : (function(done) {
        setTimeout(function() {
            gulp.src(PATH.BUILD.root + '/*.html', {read: false, force: true})
                .pipe(clean());
            done();
        }, 2000);
    }),
    "html:build" : (function() {
        gulp.src(PATH.SRC.html)
            .pipe(injectPartials())
            .pipe(gulp.dest(PATH.BUILD.root));
    })
});
series.registerSeries("html", ["html:clean", "html:build"]);

series.registerTasks({
    "css:clean" : (function(done) {
        setTimeout(function() {
            gulp.src(PATH.BUILD.css + '/*.css', {read: false, force: true})
                .pipe(clean());
            done();
        }, 1000);
    }),
    "css:build" : (function() {
        gulp.src(PATH.SRC.css)
            .pipe(gulp.dest(PATH.BUILD.css));
    })
});
series.registerSeries("css", ["css:clean", "css:build"]);

series.registerTasks({
    "js:clean" : (function(done) {
        setTimeout(function() {
            gulp.src(PATH.BUILD.js + '/*.js', {read: false, force: true})
                .pipe(clean());
            done();
        }, 1000);
    }),
    "js:build" : (function() {
        gulp.src(PATH.SRC.js)            
            .pipe(concat('main.js'))     
            .pipe(browserify())       
            .pipe(gulp.dest(PATH.BUILD.js));
    })
});
series.registerSeries("js", ["js:clean", "js:build"]);

gulp.task('images', function(){
    return gulp.src(PATH.SRC.img)
        .pipe(newer(PATH.BUILD.img))
        .pipe(imagemin())
        .pipe(gulp.dest(PATH.BUILD.img));
});

series.registerTasks({
    "build" : (function(done) {
        setTimeout(function() {
            runSequence('html', 'css', 'js', 'images');
            done();
        }, 1000);
    }),
    "watch" : (function(done){
        setTimeout(function() {
            gulp.watch([PATH.SRC.html],['html']);
            gulp.watch([PATH.SRC.css],['css']);
            gulp.watch([PATH.SRC.js],['js']);
            gulp.watch([PATH.SRC.img],['images']);
            done();
        }, 2000);
    }),
    "serve" : (function(done){
        setTimeout(function() {
            browserSync.init([PATH.BUILD.root + '/*.html',PATH.BUILD.css + '/*.css',PATH.BUILD.js + '/*.js' ], {
                server: {
                baseDir : PATH.BUILD.root
                }
            });
            done();
        }, 2000);
    }),
    "msg" : (function(done){
        console.log('----------------------------------------');
        console.log('--- Use " ctrl+C " to stop the serve ---');
        console.log('----------------------------------------');
    })
        
});
series.registerSeries("default", ["build", "serve", "watch", "msg"]);



//---------------- RELEASE ----------------------------------------------------------- 
series.registerTasks({
    "release:clean" : (function(done) {
        setTimeout(function() {
            gulp.src(PATH.RELEASE.root, {read: false, force: true})
                .pipe(clean());
            console.log("clean is done");
            done();
        }, 2000);
    }),
    "release:html" : (function(done) {
        setTimeout(function() {
            gulp.src(PATH.SRC.html)
                .pipe(injectPartials())
                .pipe(htmlmin({collapseWhitespace:true}))
                .pipe(gulp.dest(PATH.RELEASE.root));
            console.log("html is done");
            done();
        }, 2000);
    }),
    "release:css" : (function(done) {
        setTimeout(function() {
            gulp.src(PATH.SRC.css)
                .pipe(concat('app.css'))
                .pipe(cssmin())
                .pipe(rename({suffix: '-min'}))
                .pipe(gulp.dest(PATH.RELEASE.css));
            console.log("css is done");
            done();
        }, 2000);
    }),
    "release:js" : (function(done) {
        setTimeout(function() {
            gulp.src(PATH.SRC.js)
                .pipe(browserify())
                .pipe(concat('main.js'))                
                .pipe(jsmin())            
                .pipe(gulp.dest(PATH.RELEASE.js));
            console.log("js is done");
            done();
        }, 2000);
    }),
    "release:images" : (function(done) {
        setTimeout(function() {
            gulp.src(PATH.SRC.img)
                .pipe(newer(PATH.RELEASE.img))
                .pipe(imagemin())
                .pipe(gulp.dest(PATH.RELEASE.img));
            console.log("images is done");
            done();
        }, 2000);
    })    
});

series.registerSeries("release", ["release:clean", "release:html", "release:css", "release:js", "release:images"]);